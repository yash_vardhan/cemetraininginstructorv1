package com.allstate.di;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
public class Owner {
    private ObjectId id;
    private String name;
    @Autowired
    @Qualifier("Cat")
    private Pet pet;
    @Autowired
    @Qualifier("Dog")
    private Pet pet2;
    public ObjectId getId() {
        return id;
    }

    public void setId(ObjectId id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Pet getPet() {
        return pet;
    }

    public void setPet(Pet pet) {
        this.pet = pet;
    }

    // public Owner(Pet pet) {
    //     this.pet = pet;
    // }
    // public Owner(Pet pet, Pet pet2) {
    //     this.pet = pet;
    //     this.pet2=pet2;
    // }
    public Owner() {
        
    }

    public Pet getPet2() {
        return pet2;
    }

    public void setPet2(Pet pet2) {
        this.pet2 = pet2;
    }
    


}
